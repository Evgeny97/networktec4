package my_tcp.socket;//Created by Evgeny on 24.10.2017.

import my_tcp.Segment;
import my_tcp.SocketState;
import my_tcp.exeptions.MySocketException;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public abstract class MyBaseSocket {
    final int maxSocketTimeout = 1000;
    final LinkedList<byte[]> receivedData = new LinkedList<>();
    InetAddress sendingInetAddress;
    int sendingPort;
    //    DatagramSocket datagramSocket;
    LinkedList<Segment> sentSegments = new LinkedList<>();
    LinkedList<Segment> receivedBufferedSegments = new LinkedList<>();
    SocketState socketState = SocketState.CLOSED;
    int sequenceNumber;
    int acknowledgmentNumber;
    int initialSequenceNumber;
    long lastReceivedMessageTime;

    MyBaseSocket(InetAddress sendingInetAddress, int sendingPort) {
        this.sendingInetAddress = sendingInetAddress;
        this.sendingPort = sendingPort;
    }

    MyBaseSocket() {
    }

    public SocketState getSocketState() {
        return socketState;
    }

    protected abstract void sendSegment(Segment segment, boolean saveToQuery);

    void sendSegment(Segment segment) {
        sendSegment(segment, true);
    }

    public void send(byte[] data) {
        if (!isReady()) {
            throw new MySocketException("Socket is closed cant send");
        }
        if (data.length <= Segment.MAX_SEGMENT_DATA_SIZE) {
            Segment sendingSegment = new Segment(false, true, false, false, sequenceNumber,
                    acknowledgmentNumber, data);
            sendSegment(sendingSegment);
            sequenceNumber += data.length;
        } else {
            for (int sentBytes = 0; sentBytes < data.length; ) {
                int size = (data.length - sentBytes > Segment.MAX_SEGMENT_DATA_SIZE) ? 1000 : data.length - sentBytes;
                byte[] dataToSend = new byte[size];
                System.arraycopy(data, sentBytes, dataToSend, 0, size);
                Segment sendingSegment = new Segment(false, true, false, false, sequenceNumber,
                        acknowledgmentNumber, dataToSend);
                sendSegment(sendingSegment);
                sequenceNumber += size;
                sentBytes += size;
            }
        }
    }

    public int receive(byte[] buffer, int length) throws InterruptedException {
        if (!isReady()) {
            throw new MySocketException("Socket is closed cant receive");
        }
        if (length < buffer.length) {
            throw new IllegalArgumentException("length < buffer.length");
        }
        synchronized (receivedData) {
            while (receivedData.isEmpty()) {
                receivedData.wait();
            }
            int readBytes = 0;
            int remainingBits = length;
            while (!receivedData.isEmpty() && readBytes < length) {
                byte[] bytesFromList = receivedData.pollFirst();

                if (remainingBits < bytesFromList.length) {
                    byte[] leftPart = new byte[bytesFromList.length - remainingBits];
                    System.arraycopy(bytesFromList, remainingBits, leftPart, 0,
                            bytesFromList.length - remainingBits);
                    receivedData.addFirst(leftPart);
                }

                int copyLength = (remainingBits > bytesFromList.length) ? bytesFromList.length : remainingBits;
                System.arraycopy(bytesFromList, 0, buffer, readBytes, copyLength);
                readBytes += copyLength;
                remainingBits -= copyLength;
            }
            return readBytes;
        }
    }

    public void close() {
        if (socketState != SocketState.CLOSED && socketState != SocketState.FINISHED &&
                socketState != SocketState.FIN_SENT && socketState != SocketState.FIN_RECEIVED) {
            Segment finSegment = new Segment(false, true, true, false, sequenceNumber, acknowledgmentNumber, null);
            sequenceNumber++;
            sendSegment(finSegment);
            socketState = SocketState.FIN_SENT;
        }
    }

    public boolean isReady() {
        return socketState == SocketState.ESTABLISHED;
    }

    public boolean isFinished() {
        return socketState == SocketState.FINISHED;
    }

    public boolean timeoutCancelCheck() {
        if (socketState == SocketState.CLOSED) {
            return false;
        }
        return System.currentTimeMillis() - lastReceivedMessageTime > 5000;
    }

    void resendSegments() {
        if (!sentSegments.isEmpty()) {
            for (Segment segment : sentSegments) {
                segment.setAcknowledgmentNumber(acknowledgmentNumber);
                System.out.println("resend segment");
                sendSegment(segment);
            }
        } else {
            Segment ackSegment = new Segment(false, true, false, false, sequenceNumber,
                    acknowledgmentNumber, null);
            //System.out.println("послал заглушку");
            sendSegment(ackSegment, false);
        }
    }

    private void gotACKSegment(int gotACKNumber) {
        sentSegments.removeIf(segment -> MyBaseSocket.this.isFirstSeqNumLessThanSecond(segment.getSequenceNumber(),
                gotACKNumber));
    }

    private boolean isFirstSeqNumLessThanSecond(int first, int second) {
        // System.out.println(((long)Integer.MAX_VALUE-Integer.MIN_VALUE)/1500); = 2863311
        if (second > Integer.MIN_VALUE + (1500 * 100)) {
            return first < second;
        } else return (first < second) || (first > Integer.MAX_VALUE - (1500 * 100));
    }


    void handleDatagramPacket(DatagramPacket receivedPacket) {
        Segment receivedSegment = new Segment(receivedPacket.getData());
        lastReceivedMessageTime = System.currentTimeMillis();

        if (receivedSegment.isRst()) {
            socketState = SocketState.FINISHED;
            System.out.println("connection refused");
            throw new MySocketException("connection refused");
        }

        if (socketState == SocketState.CLOSED) {
            if (receivedSegment.isSyn()) {
                socketState = SocketState.SYN_RECEIVED;
                sendingInetAddress = receivedPacket.getAddress();
                sendingPort = receivedPacket.getPort();
                initialSequenceNumber = new Random().nextInt();
                sequenceNumber = initialSequenceNumber + 1;
                acknowledgmentNumber = receivedSegment.getSequenceNumber() + 1;
                Segment synAckSegment = new Segment(true, true, false, false,
                        initialSequenceNumber, acknowledgmentNumber, null);
                sendSegment(synAckSegment);
            }
            return;
        } else if (socketState == SocketState.SYN_SENT) {

            if (receivedSegment.isSyn() && receivedSegment.isAck()) {
                acknowledgmentNumber = receivedSegment.getSequenceNumber() + 1;
                socketState = SocketState.ESTABLISHED;
                System.out.println("connection established");
                Segment ackSegment = new Segment(false, true, false, false, sequenceNumber,
                        acknowledgmentNumber, null);
                sendSegment(ackSegment, false);
            }
            return;
        }

        if (receivedSegment.getSequenceNumber() == acknowledgmentNumber) {
            try {
                workWithReceivedSegment(receivedSegment);
                boolean findFitSegment = true;
                while (findFitSegment) {
                    findFitSegment = false;
                    for (Iterator<Segment> iterator = receivedBufferedSegments.iterator(); iterator.hasNext(); ) {
                        Segment segment = iterator.next();
                        if (segment.getSequenceNumber() == acknowledgmentNumber) {
                            findFitSegment = true;
                            iterator.remove();
                            workWithReceivedSegment(segment);
                        }
                    }
                }
            } catch (MySocketException e) {
                System.out.println(e.getMessage());
            }
        } else {
            if (receivedSegment.isAck() && !receivedSegment.isFin() && receivedSegment.getDataLenght() == 0) {
                gotACKSegment(receivedSegment.getAcknowledgmentNumber());
            } else {
                receivedBufferedSegments.add(receivedSegment);
            }
        }
    }


    void workWithReceivedSegment(Segment receivedSegment) {
        if (receivedSegment.isAck()) {
            gotACKSegment(receivedSegment.getAcknowledgmentNumber());
        }

        if (receivedSegment.isFin()) {
            if (socketState == SocketState.FIN_SENT) {
                socketState = SocketState.FIN_RECEIVED;
                System.out.println("fin received");
                acknowledgmentNumber++;
                Segment ackSegment = new Segment(false, true, false, false, sequenceNumber, acknowledgmentNumber, null);
                sendSegment(ackSegment, false);
            } else if (socketState != SocketState.FIN_RECEIVED) {
                socketState = SocketState.FIN_RECEIVED;
                System.out.println("fin received");
                acknowledgmentNumber++;
                Segment finSegment = new Segment(false, true, true, false, sequenceNumber, acknowledgmentNumber, null);
                sendSegment(finSegment);
                sequenceNumber++;
            }
        }

        if (socketState == SocketState.ESTABLISHED) {
            putSegmentToReceivedData(receivedSegment);
            acknowledgmentNumber += receivedSegment.getDataLenght();
            Segment ackSegment = new Segment(false, true, false, false, sequenceNumber, acknowledgmentNumber, null);
            sendSegment(ackSegment, false);
        } else if (socketState == SocketState.FIN_RECEIVED) {
            if (sentSegments.isEmpty()) {
                System.out.println("socket finished");
                socketState = SocketState.FINISHED;
            }
        } else if (socketState == SocketState.SYN_RECEIVED) {
            if (receivedSegment.isAck()) {
                socketState = SocketState.ESTABLISHED;
                System.out.println("connection established");
            }
        }
    }

    void addSegmentToSentList(Segment segment) {
        if (!sentSegments.contains(segment)) {
            sentSegments.addLast(segment);
        }
    }

    private void putSegmentToReceivedData(Segment receivedSegment) {
        if (receivedSegment.getDataLenght() != 0) {
            synchronized (receivedData) {
                receivedData.addLast(receivedSegment.getData());
                if (receivedData.size() == 1) {
                    receivedData.notifyAll();
                }
            }
        }
    }
}
