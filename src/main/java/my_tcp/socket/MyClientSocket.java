package my_tcp.socket;//Created by Evgeny on 15.10.2017.

import my_tcp.Segment;
import my_tcp.SocketState;

import java.io.IOException;
import java.net.*;
import java.util.Random;

public class MyClientSocket extends MyBaseSocket implements Runnable {
    private Thread thread = new Thread(this);
    private DatagramSocket datagramSocket;

    public MyClientSocket(int port) {
        try {
            datagramSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    public void run() {
        startWork();
    }


    private void startWork() {
        while (!thread.isInterrupted() && socketState != SocketState.FINISHED) {
            try {
                receiveSegments();
                if(timeoutCancelCheck()){
                    System.out.println("нет ответа от сервера. закрытие сокета");
                    break;
                }
                resendSegments();
            } catch (InterruptedException e) {
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void sendSegment(Segment segment, boolean saveToQuery) {
        if (saveToQuery) {
            addSegmentToSentList(segment);
        }
        byte[] buf = segment.toArray();
        try {
            datagramSocket.send(new DatagramPacket(buf, buf.length, sendingInetAddress, sendingPort));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void connect(InetAddress sendingInetAddress, int sendingPort) {
        if (socketState == SocketState.CLOSED) {
            socketState = SocketState.SYN_SENT;
            this.sendingInetAddress = sendingInetAddress;
            this.sendingPort = sendingPort;
            initialSequenceNumber = new Random().nextInt();
            sequenceNumber = initialSequenceNumber + 1;
            Segment synSegment = new Segment(true, false, false, false, initialSequenceNumber,
                    0, null);
            sendSegment(synSegment);
            lastReceivedMessageTime = System.currentTimeMillis();
            thread.start();
        }
    }

    private void receiveSegments() throws IOException, InterruptedException {
        int timeout;
        long startTime = System.currentTimeMillis();
        while ((timeout = maxSocketTimeout - (int) (System.currentTimeMillis() - startTime)) > 0) {
            DatagramPacket receivedPacket = getDatagramPacket(timeout);
            if (receivedPacket == null) {
                break;
            }
           // System.out.println("че то получил");
            handleDatagramPacket(receivedPacket);
        }
    }

    protected DatagramPacket getDatagramPacket(int timeout) throws IOException {
        datagramSocket.setSoTimeout(timeout);
        byte[] recBuffer = new byte[1500];
        DatagramPacket receivedPacket = new DatagramPacket(recBuffer, recBuffer.length);
        try {
            datagramSocket.receive(receivedPacket);
        } catch (SocketTimeoutException e) {
            return null;
        }
        return receivedPacket;
    }


}
