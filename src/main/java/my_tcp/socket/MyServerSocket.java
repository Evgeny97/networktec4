package my_tcp.socket;//Created by Evgeny on 15.10.2017.

import my_tcp.Segment;
import my_tcp.SocketState;
import my_tcp.exeptions.MySocketException;

import java.io.IOException;
import java.net.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class MyServerSocket extends Thread {
    HashMap<InetSocketAddress, MyServerChildSocket> childSockets = new HashMap<>();
    private SocketState socketState = SocketState.CLOSED;
    private DatagramSocket datagramSocket;
    private int maxSocketTimeout = 1000;
    private BlockingQueue<DatagramPacket> synDatagramPackets = new LinkedBlockingQueue<>();

    public MyServerSocket(int port) {
        try {
            datagramSocket = new DatagramSocket(port);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (!isInterrupted()) {
            try {
                receivingPackets();
                for (MyServerChildSocket childSocket : childSockets.values()) {
                    childSocket.resendSegments();
                }
                for (Iterator<MyServerChildSocket> iterator = childSockets.values().iterator(); iterator.hasNext(); ) {
                    MyServerChildSocket childSocket = iterator.next();
                    if(childSocket.isFinished()){
                        iterator.remove();
                        continue;
                    }
                    if(childSocket.timeoutCancelCheck()){
                        iterator.remove();
                        System.out.println("child socket закрыт потому что 5с нет ответа от собеседника");
                    }
                }
            } catch (InterruptedException e) {
                System.out.println("ServerSocket was Interrupted");
                break;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void receivingPackets() throws IOException, InterruptedException {
        int timeout;
        long startTime = System.currentTimeMillis();
        while ((timeout = maxSocketTimeout - (int) (System.currentTimeMillis() - startTime)) > 0) {
            datagramSocket.setSoTimeout(timeout);
            byte[] recBuffer = new byte[1500];
            DatagramPacket receivedPacket = new DatagramPacket(recBuffer, recBuffer.length);
            try {
                datagramSocket.receive(receivedPacket);
            } catch (SocketTimeoutException e) {
                break;
            }
            MyServerChildSocket targetChildSocket = childSockets.get(new InetSocketAddress(receivedPacket.getAddress(),
                    receivedPacket.getPort()));
            if (targetChildSocket != null) {
                targetChildSocket.handleDatagramPacket(receivedPacket);
            } else if (new Segment(receivedPacket.getData()).isSyn()) {
                if (!synDatagramPackets.offer(receivedPacket, 500, TimeUnit.MILLISECONDS)) {
                    System.out.println("очередь syn пакетов заблокирована не удалось положить пакет");
                }
            }

        }
    }

    public void listen() {
        socketState = SocketState.LISTEN;
        this.start();
    }


    public void sendDatagramPacket(DatagramPacket datagramPacket) {
        try {
            datagramSocket.send(datagramPacket);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MyServerChildSocket accept() throws InterruptedException {
        if (socketState == SocketState.LISTEN) {
            DatagramPacket synDatagramPacket = synDatagramPackets.take();
            System.out.println("Create new Child socket");
            MyServerChildSocket newChildSocket = new MyServerChildSocket(synDatagramPacket.getAddress(),
                    synDatagramPacket.getPort(), this);
            childSockets.put(new InetSocketAddress(synDatagramPacket.getAddress(), synDatagramPacket.getPort()),
                    newChildSocket);
            newChildSocket.handleDatagramPacket(synDatagramPacket);
            return newChildSocket;
        } else {
            throw new MySocketException("SocketState!=Listen");
        }
    }
}
