package my_tcp.socket;//Created by Evgeny on 15.10.2017.

import my_tcp.Segment;

import java.net.DatagramPacket;
import java.net.InetAddress;

public class MyServerChildSocket extends MyBaseSocket {
    MyServerSocket myServerSocket;

    MyServerChildSocket(InetAddress sendingInetAddress, int sendingPort, MyServerSocket myServerSocket) {
        super(sendingInetAddress, sendingPort);
        this.myServerSocket = myServerSocket;
        lastReceivedMessageTime = System.currentTimeMillis();
    }

    @Override
    protected void sendSegment(Segment segment, boolean saveToQuery) {
        if (saveToQuery) {
            addSegmentToSentList(segment);
        }
        byte[] buf = segment.toArray();
        myServerSocket.sendDatagramPacket(new DatagramPacket(buf, buf.length, sendingInetAddress, sendingPort));
    }


}
