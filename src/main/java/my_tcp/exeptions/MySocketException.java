package my_tcp.exeptions;//Created by Evgeny on 15.10.2017.

public class MySocketException extends RuntimeException {
    public MySocketException() {
    }

    public MySocketException(String message) {
        super(message);
    }
}
