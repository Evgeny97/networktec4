package my_tcp;//Created by Evgeny on 15.10.2017.

public enum SocketState {
    CLOSED, LISTEN, SYN_SENT,SYN_RECEIVED,ESTABLISHED, FIN_SENT, FIN_RECEIVED,FINISHED
}
