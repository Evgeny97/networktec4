package my_tcp;

import my_tcp.socket.MyServerChildSocket;
import my_tcp.socket.MyServerSocket;

import java.io.IOException;
import java.net.UnknownHostException;

public class UserIOThreadServer extends Thread {
    @Override
    public void run() {
        try {
            MyServerSocket serverSocket = new MyServerSocket(4000);
            serverSocket.listen();
            MyServerChildSocket myServerChildSocket = serverSocket.accept();
            while (!myServerChildSocket.isReady()) {
                Thread.currentThread().sleep(250);
            }
            byte[] buff = new byte[1000];
            myServerChildSocket.receive(buff, 1000);
            System.out.write(buff);

            buff = new byte[1000];
            int read = System.in.read(buff);
            myServerChildSocket.send(buff);



        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            return;
        }
    }
}
