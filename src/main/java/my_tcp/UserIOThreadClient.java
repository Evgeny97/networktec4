package my_tcp;

import my_tcp.socket.MyClientSocket;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class UserIOThreadClient extends Thread {
    @Override
    public void run() {
        try {
            MyClientSocket socket = new MyClientSocket(4001);
            socket.connect(InetAddress.getByName("localhost"), 4000);

            byte[] buff = new byte[1000];
            int read = System.in.read(buff);
            socket.send(buff);

            buff = new byte[1000];
            socket.receive(buff, 1000);
            System.out.write(buff);

            socket.close();


        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            return;
        }
    }
}
