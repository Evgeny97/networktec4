package my_tcp;//Created by Evgeny on 15.10.2017.

import java.nio.ByteBuffer;

public class Segment {
    public static final int MAX_SEGMENT_DATA_SIZE = 1000;
    /*
    from left to right
    0- SYN
    1- ACK
    2- FIN
    3- RST
    */
    private byte flagMask;
    //private byte headerLength;
    private int sequenceNumber;
    private int acknowledgmentNumber;
    private int dataLenght;
    private boolean syn;
    private boolean ack;
    private boolean fin;
    private boolean rst;
    private byte[] data;

    public Segment(boolean syn, boolean ack, boolean fin, boolean rst, int sequenceNumber, int acknowledgmentNumber,
                   byte[] data) {
        setMaskFromFlags(syn, ack, fin, rst);
        this.sequenceNumber = sequenceNumber;
        this.acknowledgmentNumber = acknowledgmentNumber;
        this.syn = syn;
        this.ack = ack;
        this.fin = fin;
        this.rst = rst;
        this.data = data;
        if (data != null) {
            dataLenght = data.length;
        } else {
            dataLenght = 0;
        }
    }

    public Segment(byte[] fullData) {
        ByteBuffer byteBuffer = ByteBuffer.wrap(fullData);
        setFlagsFromMask(byteBuffer.get());
        this.sequenceNumber = byteBuffer.getInt();
        this.acknowledgmentNumber = byteBuffer.getInt();
        this.dataLenght = byteBuffer.getInt();
        if (dataLenght != 0) {
            data = new byte[dataLenght];
            byteBuffer.get(data);
        } else {
            data = null;
        }

    }

    public int getDataLenght() {
        return dataLenght;
    }

    public byte[] getData() {

        return data;
    }

    public int getAcknowledgmentNumber() {
        return acknowledgmentNumber;
    }

    public void setAcknowledgmentNumber(int acknowledgmentNumber) {
        ack = true;
        this.acknowledgmentNumber = acknowledgmentNumber;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public boolean isSyn() {
        return syn;
    }

    public boolean isAck() {
        return ack;
    }

    public boolean isFin() {
        return fin;
    }

    public boolean isRst() {
        return rst;
    }

    private void setFlagsFromMask(byte mask) {
        syn = (mask & 1) != 0;
        ack = (mask & 2) != 0;
        fin = (mask & 4) != 0;
        rst = (mask & 8) != 0;
    }

    private void setMaskFromFlags(boolean syn, boolean ack, boolean fin, boolean rst) {
        flagMask = 0;
        if (rst) {
            flagMask++;
        }
        flagMask <<= 1;
        if (fin) {
            flagMask++;
        }
        flagMask <<= 1;
        if (ack) {
            flagMask++;
        }
        flagMask <<= 1;
        if (syn) {
            flagMask++;
        }
    }

    public byte[] toArray() {
        ByteBuffer byteBuffer = ByteBuffer.allocate(13 + dataLenght)
                .put(flagMask)
                .putInt(sequenceNumber)
                .putInt(acknowledgmentNumber)
                .putInt(dataLenght);
        if (data != null) {
            byteBuffer.put(data);
        }
        return byteBuffer.array();
    }


}
