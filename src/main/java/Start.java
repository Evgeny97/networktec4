import my_tcp.UserIOThreadClient;
import my_tcp.UserIOThreadServer;

public class Start {
    public static void main(String[] args) {
        if(args[0].equals("server")){
            UserIOThreadServer userIOThreadServer = new UserIOThreadServer();
            userIOThreadServer.start();
        }
        else if(args[0].equals("client")){
            UserIOThreadClient userIOThreadClient = new UserIOThreadClient();
            userIOThreadClient.start();
        }else {
            System.out.println("invalid arguments");
        }
    }
}
